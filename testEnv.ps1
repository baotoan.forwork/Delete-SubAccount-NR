$params = @{
    # Uri         = 'https://rpm.newrelic.com/api/v2/partners/1673/accounts/$env:ACCOUNT_ID'
    Uri         = "https://rpm.newrelic.com/api/v2/partners/1673/accounts/$env:ACCOUNT_ID"
    Headers     = @{"X-API-Key"= "$env:PARTNERSHIP_API_KEY"}
    Method      = 'DELETE'
    ContentType = 'application/json'
}
# $response = 'https://rpm.newrelic.com/api/v2/partners/1673/accounts/"$env:ACCOUNT_ID"'
$Headers = @{"X-API-Key"= "$env:PARTNERSHIP_API_KEY"}
Write-Output $Headers
Write-Output $params

# Invoke-RestMethod -Uri "https://rpm.newrelic.com/api/v2/partners/1673/accounts/$env:ACCOUNT_ID" -Headers @{"X-API-Key"= "$env:PARTNERSHIP_API_KEY"} -Method 'DELETE' -ContentType "application/json"