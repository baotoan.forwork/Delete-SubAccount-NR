locals {
  module_path = replace("${path.module}", "/", "\\")
}

module "newrelic_account_delete" {
  source          = "matti/resource/shell"
  version         = "1.4.0"
  command_windows = "${local.module_path}\\testEnv.ps1"
  command_when_destroy_windows = "${local.module_path}\\deleteSubAccount.ps1"

  environment = {
    "ACCOUNT_ID" : "3280933" ,
    "PARTNERSHIP_API_KEY" : "NRRA-1d906bd0903f02163ef7bcc20a7c0446ffedabc870"
  }
}

output "test" {
  value = module.newrelic_account_delete.stdout
}
