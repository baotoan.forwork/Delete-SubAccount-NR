[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12 #Force using Tls1.2

$params = @{
    Uri         = "https://rpm.newrelic.com/api/v2/partners/1673/accounts/$env:ACCOUNT_ID"
    Headers     = @{"X-API-Key"= "$env:PARTNERSHIP_API_KEY"}
    Method      = 'DELETE'
    ContentType = 'application/json'
}
Invoke-RestMethod @params
